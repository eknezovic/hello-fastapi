from datetime import date
from dash import Dash, html, dcc
from dash.dependencies import Input, Output

import plotly.express as px


import pandas as pd

from database import SQLALCHEMY_DATABASE_URL

df = pd.read_sql_table('event', SQLALCHEMY_DATABASE_URL)


app = Dash(__name__)
app.layout = html.Div([
    dcc.DatePickerSingle(
        id='my-date-picker-single',
        min_date_allowed=date(1900, 8, 5),
        max_date_allowed=date(2022, 4, 13),
        initial_visible_month=date(2022, 4, 13),
        date=date(1952, 4, 13)
    ),
    dcc.Graph(id='graph-with-slider'),
])


@app.callback(
    Output('graph-with-slider', 'figure'),
    Input('my-date-picker-single', 'date'))
def update_output(date_value):



    fig = px.bar(df, x='year', y='pop', color='year')

    fig.update_layout(transition_duration=500)
    
    return fig



if __name__ == '__main__':
    app.run_server(debug=True)