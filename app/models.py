import datetime

import sqlalchemy
import sqlalchemy.orm
from sqlalchemy.sql import func


import app.database as database

import pytz
tz = pytz.timezone('Europe/Prague')



"""
#### Models

- Result (id, puzzle_id, batch.id, date_of_start, date_of_end, win)
- Batch (id, date, number, date_of_creation)
"""

class BaseModel(database.Base):
    __abstract__ = True
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True, index=True, autoincrement=True)
    created_at = sqlalchemy.Column(sqlalchemy.DateTime, default=datetime.datetime.now(tz=tz))
    updated_at = sqlalchemy.Column(sqlalchemy.DateTime, default=datetime.datetime.now(tz=tz), onupdate=func.now())


"""
class Sri(BaseModel):
    __tablename__ = "sri"
    assign_to_new = sqlalchemy.Column(sqlalchemy.Boolean, default=False)
    levels = sqlalchemy.orm.relationship("SriLevel")

class SriLevel(BaseModel):
    __tablename__ = "sri_level"
    sri_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey(Sri.id))
    level = sqlalchemy.Column(sqlalchemy.Integer)
    nr_of_days = sqlalchemy.Column(sqlalchemy.Integer)


class Puzzle(BaseModel):
    __tablename__ = "puzzle"
    id = sqlalchemy.Column(sqlalchemy.String, primary_key=True, index=True)
"""

puzzle_to_theme_table = sqlalchemy.Table(
    'PuzzleToTheme', 
    database.Base.metadata,
    sqlalchemy.Column('puzzle_id', sqlalchemy.ForeignKey('puzzle.id'), primary_key=True),
    sqlalchemy.Column('theme_id', sqlalchemy.ForeignKey('theme.id'), primary_key=True),
)


from app.enums import EventEnum

class User(BaseModel):
    __tablename__ = "user"
    username = sqlalchemy.Column(sqlalchemy.String, nullable=False, index=True, unique=True)
    hashed_password = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    results = sqlalchemy.orm.relationship("Result", back_populates="user")


class Event(BaseModel):
    """
    timestamp
    event_type
    text
    session_id
    session
    """
    __tablename__ = "event"
    timestamp = sqlalchemy.Column(sqlalchemy.DateTime, nullable=False, index=True)
    event = sqlalchemy.Column(sqlalchemy.Enum(EventEnum), nullable=False)
    text = sqlalchemy.Column(sqlalchemy.String)
    session_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('session.id'))
    session = sqlalchemy.orm.relationship("Session", back_populates="events")

    def __repr__(self):
        return "Hi"

    def __str__(self):
        return "Hello"

    def str(self):
        return "Bye"

from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
import math

class Session(BaseModel):
    __tablename__ = "session"
    puzzle_id = sqlalchemy.Column(sqlalchemy.String, sqlalchemy.ForeignKey('puzzle.id'))
    text = sqlalchemy.Column(sqlalchemy.String)
    puzzle = sqlalchemy.orm.relationship("Puzzle", back_populates="sessions")
    events = sqlalchemy.orm.relationship("Event", back_populates="session")

class Result(BaseModel):
    __tablename__ = "result"
    puzzle_id = sqlalchemy.Column(sqlalchemy.String, sqlalchemy.ForeignKey('puzzle.id'), index=True)
    user_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('user.id'), index=True)
    start_time = sqlalchemy.Column(sqlalchemy.DateTime)
    end_time = sqlalchemy.Column(sqlalchemy.DateTime)
    win = sqlalchemy.Column(sqlalchemy.Boolean)
    user = sqlalchemy.orm.relationship("User", back_populates="results")
    puzzle = sqlalchemy.orm.relationship("Puzzle", back_populates="results")

    @hybrid_property
    def total_seconds(self):
        return round((self.end_time - self.start_time).total_seconds(), 1)


class Scheduled(BaseModel):
    __tablename__ = "scheduled"
    puzzle_id = sqlalchemy.Column(sqlalchemy.String, sqlalchemy.ForeignKey('puzzle.id'), index=True)
    date = sqlalchemy.Column(sqlalchemy.Date)
    puzzle = sqlalchemy.orm.relationship("Puzzle", back_populates="schedules")

class Puzzle(BaseModel):
    __tablename__ = "puzzle"
    id = sqlalchemy.Column(sqlalchemy.String, primary_key=True, index=True)
    fen = sqlalchemy.Column(sqlalchemy.String)
    moves = sqlalchemy.Column(sqlalchemy.String)
    rating = sqlalchemy.Column(sqlalchemy.Integer)
    rating_deviation = sqlalchemy.Column(sqlalchemy.Integer)
    popularity = sqlalchemy.Column(sqlalchemy.Integer)
    nb_plays = sqlalchemy.Column(sqlalchemy.Integer)
    game_url = sqlalchemy.Column(sqlalchemy.String)
    themes = sqlalchemy.orm.relationship("Theme", secondary=puzzle_to_theme_table, back_populates="puzzles")
    results = sqlalchemy.orm.relationship("Result", back_populates="puzzle")
    schedules = sqlalchemy.orm.relationship("Scheduled", back_populates="puzzle")
    sessions = sqlalchemy.orm.relationship("Session", back_populates="puzzle")

class Theme(BaseModel):
    __tablename__ = "theme"
    name = sqlalchemy.Column(sqlalchemy.String, index=True, unique=True)
    puzzles = sqlalchemy.orm.relationship("Puzzle", secondary=puzzle_to_theme_table, back_populates="themes", lazy='dynamic')

    def __str__(self):
        return self.name

