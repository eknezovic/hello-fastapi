

function get_timestamp() {
    return Date.now()
}

class EventStorage {
    constructor() {
        this.events = []
        this.sessionId = null
        this.puzzleId = null
    }

    addEvent(event_name, timestamp = null, text = null) {
        if (timestamp === null) {
            timestamp = get_timestamp()
        }
        const event = {
            'event': event_name.toUpperCase(),
            'timestamp': timestamp,
            'text': text
        }
        this.events.push(event)
        console.log(event)
    }

    clear() {
        this.events = []
    }
}


var start_time
var end_time
var win

eventStorage = new EventStorage()



// const breakText = prompt("What did you do since the last chess puzzle?")
// eventStorage.addEvent("label_break_activity", null, breakText)

async function run() {


    function getToken(cookie) {
        cookie = decodeURI(cookie)
        console.log("COOKIE")
        console.log(cookie)
        var retval = ""
        cookie.split("; ").forEach(key_value => {
            console.log("KV: ", key_value)
            var key, value = key_value.split("=")
            console.log(value)
            if (value[0] === "session_token") {
                console.log("FOUND TOKEN")
                console.log(value[1])
                retval = value[1]
            }
        })
        return retval;
    }

    const token = getToken(document.cookie)

    headers = {
        'Content-Type': 'application/json',
        'Authorization': "Bearer " + token
    }
    console.log("Headers: ", headers)


    // Create urlParams query string
    var urlParams = new URLSearchParams(window.location.search);
    // Get value of single parameter
    var param_puzzle_id = urlParams.get('id');
    var getPuzzleUrl = "/api/get-puzzle"
    console.log("PARAM PUZZLE ID:")
    console.log(param_puzzle_id)
    var param_min_rating = urlParams.get('min_rating');
    var param_max_rating = urlParams.get('max_rating');
    var data, response;
    if (param_puzzle_id) {
        getPuzzleUrl = getPuzzleUrl + "?id=" + param_puzzle_id
    } else if (param_min_rating && param_max_rating) {
        var getRandomPuzzleUrl = "/api/get-random-puzzle"
        getRandomPuzzleUrl += "?min_rating=" + param_min_rating + "&max_rating=" + param_max_rating
        var response = await fetch(getRandomPuzzleUrl, {
            headers: headers
        })
        data = await response.json()
        getPuzzleUrl = getPuzzleUrl + "?id=" + data.puzzle_id
    }
    console.log("Hello?")
    console.log(Date.now())
    console.log("Headers: ", headers)
    console.log("AAAAAAA")
    response = await fetch(getPuzzleUrl, {
        headers: headers
    })
    console.log(Date.now())
    data = await response.json()

    eventStorage.puzzleId = data.puzzle_id
    eventStorage.sessionId = data.session_id

    document.getElementById("before").classList.add("hidden")
    document.getElementById("during").classList.remove("hidden")
    return await run_chess_puzzle(data)
}


async function run_pre() {


}

async function run_chess_puzzle(data) {


    var fenPosition = data.fen
    var theMoves = data.moves.split(" ")

    let inputTextField = document.getElementById("candidateMoveInput")
    inputTextField.onkeyup = handleKeyPress
    inputTextField.oninput = handleInput
    inputTextField.focus()

    var oldInput = ""
    var isOver = false
    var currentIndexMove = 0
    var playerIsWhite = fenPosition.split(" ")[1] != "w"
    var candidateMove = null

// NOTE: this example uses the chess.js library:
// https://github.com/jhlywa/chess.js

    var board = null
    var game = new Chess(fenPosition)

    eventStorage.addEvent('puzzle_start')
    start_time = get_timestamp()
    // await sendPayload()

    function onDragStart(source, piece, position, orientation) {
        // do not pick up pieces if the game is over
        if (isOver || game.game_over()) return false

        if (playerIsWhite) {
            // only pick up pieces for White
            if (piece.search(/^b/) !== -1) return false
        } else {
            // only pick up pieces for Black
            if (piece.search(/^b/) !== 0) return false
        }

    }

    function makeMove() {
        if (isOver || game.game_over()) return false

        var theMove = null
        theMove = theMoves[currentIndexMove]
        currentIndexMove += 1
        const x = {
            from: theMove.slice(0, 2),
            to: theMove.slice(2, 4)
        }
        game.move(x)
        board.position(game.fen())
    }


    async function onDrop(source, target) {
        await setCandidateMove(source, target)
    }
    
    async function submitReturnToFocus() {
    }

    async function setCandidateMove(source, target, isOnDrop=true) {
        move = {
            from: source,
            to: target,
            promotion: 'q' // NOTE: always promote to a queen for example simplicity
        }

        var fen = game.fen()
        // see if the move is legal
        var move = game.move(move)
        console.log("MOVE:")
        console.log(move)
        // illegal move
        if (move === null) {
            console.log("Invalid!!!", candidateMove)
            return false
        } else {
            if (false) {
            const candidateMoveTextDisplay = candidateMove.from + " -> " + candidateMove.to
            const candidateMoveTextInput = candidateMove.from + candidateMove.to
            eventStorage.addEvent("candidate_move_set", null, candidateMoveTextInput)
            document.getElementById("candidateMoveInput").value = candidateMoveTextInput
            document.getElementById("candidateMoveDisplay").innerText = candidateMoveTextDisplay
            document.getElementById("myBoard").style.border = "thick solid orange";
            document.getElementById("myBoard").style.borderWidth = "0px 10px 0px 10px";
            return true
            } else {
                // game.load(fen) // return to the previous state, we don't want to make a move here!
                if (!isOnDrop) {
                    board.position(game.fen()) // new position
                }
                console.log("Candidate move is valid: ", candidateMove)
                await executeCandidateMove(move)
            }
        }
    }

    async function submitCandidateMove(textInput) {
        console.log(textInput)
        const source = textInput.substring(0, 2)
        const target = textInput.substring(2, 4)
        console.warn(source)
        console.warn(target)
        await setCandidateMove(source, target, false)
    }

    async function executeCandidateMove(userMovement) {
        const candidateMoveTextInput = userMovement.from + userMovement.to
        eventStorage.addEvent("candidate_move_execute", null, candidateMoveTextInput)
        // const userMovement = game.move(candidateMove)
        // clearCandidateMove(false)

        var isRightMove = null
        theMove = theMoves[currentIndexMove]
        currentIndexMove += 1
        const expectedMovement = {
            from: theMove.slice(0, 2),
            to: theMove.slice(2, 4),
            promotion: 'q'
        }
        console.info(expectedMovement)
        console.info(userMovement)
        const isLast = currentIndexMove === theMoves.length
        if (game.in_checkmate() || expectedMovement.from === userMovement.from && expectedMovement.to === userMovement.to) {
            isRightMove = true
            if (isLast || game.in_checkmate()) {
                isOver = true
            }
        } else {
            isRightMove = false
            isOver = true
        }
        if (!isOver) {
            window.setTimeout(makeMove, 500)
            // makeMove()
        } else {
            end_time = get_timestamp()
            if (isRightMove) {
                eventStorage.addEvent('puzzle_end_win')
                document.getElementById("myBoard").style.border = "thick solid green";
                document.getElementById("myBoard").style.borderWidth = "20px 20px 20px 20px";
                win = true
            } else {
                eventStorage.addEvent('puzzle_end_lose')
                document.getElementById("myBoard").style.border = "thick solid red";
                document.getElementById("myBoard").style.borderWidth = "20px 20px 20px 20px";
                win = false
            }
            console.log("Should send payload here...")
            await sendPayload()
            moveToNextState()
        }
    }

    async function handleKeyPress(e) {
        if (e.key === 'Enter') {
            // await executeCandidateMove()
            // clearCandidateMove(false)
        }
    }

    async function handleInput(e) {
        const newInput = e.target.value
        if (oldInput.length >= 4 && newInput.length >= 5)  {
            e.target.value = oldInput.slice(0, 4)
        }
        else if (oldInput.length == 4 && newInput.length == 3) {
            // clearCandidateMove(true)
            e.target.value = ""
        } else if (oldInput.length == 3 && newInput.length == 4) {
            outcome = await submitCandidateMove(newInput)
            if (outcome) {
                console.log("")
            } else {
                // clearCandidateMove(false)
                e.target.value = ""
            }
        }

        oldInput = newInput
    }

    async function sendPayload() {
        const storeResultUrl = "/api/store-puzzle-result"

        // TODO: use the data endpoint to connect the result to user

        const data = { 
            'session_id': eventStorage.sessionId,
            'puzzle_id': eventStorage.puzzleId,
            'events': eventStorage.events
        }

        const new_data = {
            'puzzle_id': eventStorage.puzzleId,
            'start_time': start_time,
            'end_time': end_time,
            'win': win,
            // 'events': eventStorage.events // TODO: determine whether to store this as well
        }

        // const body = JSON.stringify(data)
        const body = JSON.stringify(new_data)

        console.log("Body to be posted: ", body)

        const options = {
            method: 'POST',
            body: body,
            headers: headers
        }

        const FEATURE_TOGGLE = true
        if (FEATURE_TOGGLE) {
            await fetch(storeResultUrl, options) 
                .then(res => res.json())
                .then(res => {
                    console.log("Data sent!")
                    eventStorage.clear()
                });
        } else {
            console.log("Would send this: ", data)
        }
    }


    async function submitSession(destination) {
        const pmText = document.getElementById("postMortemInput").value
        eventStorage.addEvent("label_post_mortem", null, pmText)

        document.getElementById("postMortemInput").value = ""

        var url
        if (destination === "puzzle") {
            url = window.location
        } else if (destination === "menu") {
            url = "/"
        } else if (destination === "solution") {
            url = "https://lichess.org/training/" + eventStorage.puzzleId
            eventStorage.addEvent("view_solution", null, url)
        }


        // await sendPayload()

        window.location.href = url

    }

    async function recall(arg) {
    }
0
// update the board position after the piece snap
// for castling, en passant, pawn promotion
    function onSnapEnd() {
        board.position(game.fen())
    }

    function moveToNextState() {
        /*
        Make a URL request to server. Server has to allow all origins tho.
         */

        document.getElementById("during").classList.add("hidden")
        document.getElementById("after").classList.remove("hidden")
        // document.getElementById("postMortemInput").focus()
        document.getElementById("nextPuzzleButton").focus()

    }

    var config = {
        draggable: true,
        position: fenPosition,
        orientation: playerIsWhite ? "white" : "black",
        // position: 'start',
        onDragStart: onDragStart,
        onDrop: onDrop,
        onSnapEnd: onSnapEnd
    }

    board = Chessboard('myBoard', config)
    window.setTimeout(makeMove, 500)
    // makeMove()

    return {
        'executeCandidateMove': executeCandidateMove,
        'submitSession': submitSession,
    }
}



