
from datetime import datetime, timedelta, timezone
from typing import List, Optional
import random

from sqlalchemy import func

import pytz

from app.pydantic_models import EventModel, EventsModel
from app.models import Result, Scheduled, Puzzle, Theme, Session, Event


tz = pytz.timezone('Europe/Prague')

def get_today(date_only=True):
    today = datetime.now(tz)
    if date_only:
        today = today.date()
    return today

def convert_int_to_date(number):
    epoch = number / 1000
    utc_date = datetime.fromtimestamp(epoch, tz=tz)  # outputs: datetime date object
    return utc_date

def store_puzzle_result(db_session, puzzle_result):
    import json
    d = json.loads(puzzle_result.json())
    d['start_time'] = convert_int_to_date(d['start_time'])
    d['end_time'] = convert_int_to_date(d['end_time'])
    result = Result(**d)
    db_session.add(result)
    db_session.commit()
    next_date = get_next_date_for_puzzle(db_session, result.puzzle_id, sri.algorithm, current_date=result.start_time)
    schedule_puzzle(db_session, result.puzzle_id, next_date)
    db_session.commit()

def get_scheduled_puzzles(db_session, _date=None):
    if _date is None:
        _date = datetime.today().date()

    # Will need a where in query below...
    result_query = db_session.query(Result).filter(Result.start_time<_date).group_by(Result.puzzle_id).having(func.max(Result.start_time)).all()
    result_to_date = {i.puzzle_id: i.start_time.date() for i in result_query}
    # print(result_to_date)

    scheduled_query = db_session.query(Scheduled).filter(Scheduled.date<_date+timedelta(days=1)).group_by(Scheduled.puzzle_id).having(func.max(Scheduled.date)).all()
    scheduled_to_date = {i.puzzle_id: i.date for i in scheduled_query}
    # print(scheduled_to_date)

    result = {puzzle_id for puzzle_id, scheduled_date in scheduled_to_date.items() if puzzle_id in result_to_date and result_to_date[puzzle_id] < scheduled_date}
    return result

def _get_scheduled_puzzles(db_session, _date=None):
    if _date is None:
        _date = datetime.today().date()
    query = db_session.query(Scheduled.puzzle_id).filter_by(date=_date)
    return set([i.puzzle_id for i in query])



def get_new_puzzle_results_only(db_session, _date):
    # What are we doing here? 
    puzzle_results = get_puzzle_results(db_session, _date)
    query = db_session.query(Result.puzzle_id)
    query = query.filter(Result.start_time < _date)
    previous_puzzle_ids = {i.puzzle_id for i in query}
    return puzzle_results.filter(Result.puzzle_id.notin_(previous_puzzle_ids))

def get_latest_new_puzzle_result(db_session, _date=None):
    # Get all results where the puzzle_id count is 1.
    # Out of all of those puzzle ids, find the latest one.
    query = db_session.query(Result.id, Result.puzzle_id, func.count(Result.puzzle_id), func.max(Result.start_time))
    if _date:
        query = query.filter_by(Result.start_time<_date)
    query = query.group_by(Result.puzzle_id).having(func.count(Result.puzzle_id)==1)\
        .order_by(func.max(Result.start_time).desc())
    latest = query.first()

    result = db_session.query(Result)
    if latest:
        _id = latest.id
        result = result.filter_by(id=_id).one()
    return result

def get_unfinished_scheduled_puzzles_set(db_session, _date=None):
    if _date is None:
        _date = datetime.today().date()
    scheduled_puzzles = get_scheduled_puzzles(db_session, _date)
    results = get_puzzle_results(db_session, _date)
    results = set([i.puzzle_id for i in results])
    unfinished = scheduled_puzzles - results
    return unfinished

def get_unfinished_scheduled_puzzles(db_session, _date=None):
    ids = get_unfinished_scheduled_puzzles_set(db_session, _date)
    puzzles = db_session.query(Puzzle).filter(Puzzle.id.in_(ids))
    return puzzles

def schedule_puzzle(db_session, puzzle_id, _date):
    scheduled = Scheduled(**{
        "puzzle_id": puzzle_id,
        "date": _date
    })
    db_session.add(scheduled)
    db_session.commit()

def get_current_streak_count(db_session, puzzle_id, _date=None):
    from sqlalchemy import desc
    query = db_session.query(Result)\
        .filter_by(puzzle_id=puzzle_id)\
        .order_by(desc(Result.start_time))
    win_streak = 0
    for i in query:
        if i.win == True:
            win_streak += 1
        else:
            break
    return win_streak

def get_next_date_for_puzzle(db_session, puzzle_id, sri_algorithm, current_date=None):
    # Get puzzle level
    # Get the number of days
    # Get next date
    win_streak = get_current_streak_count(db_session, puzzle_id, current_date)
    day_offset = sri_algorithm(win_streak=win_streak)
    return current_date + timedelta(days=day_offset)

def add_puzzles(db_session, attrs_list, commit_at_end=True):
    from sqlalchemy.orm.exc import NoResultFound
    from sqlalchemy.orm.exc import MultipleResultsFound
    existing_ids = {i[0] for i in db_session.query(Puzzle.id).all()}
    all_themes = {theme.name: theme for theme in db_session.query(Theme).all()}
    print(all_themes)
    puzzle_objects = []
    attrs_list = [attrs for attrs in attrs_list if attrs['id'] not in existing_ids]
    if not attrs_list:
        print("Batch was already added... skipping!")
        return
    else:
        print(f"Batch will get inputted... size: {len(attrs_list)}")
    for attrs in attrs_list:
        themes = attrs['themes']
        theme_objs = []
        for theme_name in themes:
            if theme_name not in all_themes:
                theme = Theme(name=theme_name)
                db_session.add(theme)
                db_session.commit()
                all_themes = {theme.name: theme for theme in db_session.query(Theme).all()}
            theme_objs.append(all_themes[theme_name])
        attrs['themes'] = theme_objs
        puzzle = Puzzle(**attrs)
        puzzle_objects.append(puzzle)
        # db_session.add(puzzle)
    db_session.add_all(puzzle_objects)
    db_session.commit()
    if commit_at_end:
        db_session.commit()
    return

def get_relevant_puzzle_rating_interval(db_session):
    # 1) Find the last "new" puzzle we solved
    # 2) if loss, return <rating-offset, rating>
    # 3) if win, return <rating, rating+offset>
    return

def get_old_puzzle_ids(db_session):
    # return {i.puzzle_id for i in db_session.query(Result)}
    return []

def get_new_relevant_puzzles(db_session, rating_offset=50):
    latest_new_puzzle_result = get_latest_new_puzzle_result(db_session)
    if not latest_new_puzzle_result:
        return []

    if latest_new_puzzle_result.win:
        min_rating = latest_new_puzzle_result.puzzle.rating
        max_rating = min_rating + rating_offset
    else:
        max_rating = latest_new_puzzle_result.puzzle.rating
        min_rating = max_rating - rating_offset

    puzzles = get_new_puzzles(db_session, min_rating=min_rating, max_rating=max_rating)
    if puzzles.count() == 0:
        print("No items were found...")
        return get_new_relevant_puzzles(db_session, rating_offset=rating_offset+rating_offset)
    return puzzles

def get_old_or_new(db_session):
    old_puzzles = get_unfinished_scheduled_puzzles(db_session)
    new_puzzles = get_new_relevant_puzzles(db_session)
    coinflip = random.randint(0, 1)
    # coinflip = 1 # hack to flush the old puzzles
    if coinflip or not new_puzzles:
        puzzles = old_puzzles
    else:
        puzzles = new_puzzles
    puzzle = random.choice(list(puzzles))
    return puzzle


# ----------

from sqlalchemy.sql.expression import func, select

def get_random_puzzle(db_session, min_rating=None, max_rating=None):
    return get_new_puzzles(db_session, min_rating=min_rating, max_rating=max_rating).order_by(func.random()).first()

def get_new_puzzles(db_session, puzzle_category=None, old_puzzle_ids=None, min_rating=None, max_rating=None):
    # 1) Get all puzzle ids
    # 2) Filter all puzzle ids from results
    # 3)
    # if not old_puzzle_ids:
        # old_puzzle_ids = get_old_puzzle_ids(db_session)
    # new_puzzles = db_session.query(Puzzle).filter(Puzzle.id.notin_(old_puzzle_ids))
    if not puzzle_category:
        new_puzzles = db_session.query(Puzzle)
    else:
        new_puzzles = puzzle_category.puzzles
    if min_rating:
        new_puzzles = new_puzzles.filter(Puzzle.rating >= min_rating)
    if max_rating:
        new_puzzles = new_puzzles.filter(Puzzle.rating <= max_rating)
    # if puzzle_category:
        # theme = db_session.query(Theme).filter(Theme.name == puzzle_category)
        # new_puzzles = new_puzzles.filter(Puzzle.themes == theme)
    return new_puzzles


def create_new_puzzle_session(db_session, puzzle):
    puzzle_session = Session(puzzle=puzzle)
    db_session.add(puzzle_session)
    db_session.commit()
    return puzzle_session

def store_events(db_session, data: EventsModel):
    puzzle_session = db_session.query(Session).get(data.session_id)
    for event in data.events:
        data = event.dict()
        data['session_id'] = puzzle_session.id # FIXME
        data['timestamp'] = convert_int_to_date(data['timestamp'])
        db_session.add(Event(**data))
    db_session.commit()
    return

def get_random_puzzle_category(db_session):
    return random.choice(list(db_session.query(Theme)))

def get_appropriate_rating(df = None):
    if df is None or df.empty or not len(df.index):
        return 1700
    df = df.head(14) # FIXME
    success_rate = get_success_rate(df)
    success_ratings = df[df['win'] == True]["rating"]
    if success_rate > 0.9:
        result = success_ratings.max() + 50
    elif success_rate < 0.8:
        result = (success_ratings.min() + success_ratings.mean())/2
    else:
        # result = (success_ratings.max() + success_ratings.mean())/2
        result = success_ratings.mean()
    last_result = df.iloc[0]
    if last_result["win"]:
        # result += 50
        print("last was won, so let's crankit up")
        if last_result["total_time"] < 20:
            print("wow, that was fast!")
            # result += 100
        result = result if result > last_result["win"] else last_result["rating"]
    return int(result)


def get_success_rate(df, last_n=14):
    df = df.head(last_n)
    return df["win"].values.sum()/len(df.index)

def get_previous_results(db, user, limit=None):
    from sqlalchemy import desc
    query = db.query(Result)\
        .filter(Result.user==user)\
        .order_by(desc(Result.start_time))
    if limit:
        query = query.limit(limit)
    return query

def load_dataframe_from_collection_of_results(collection_of_results):
    import pandas as pd
    columns = ["rating", "total_time", "start_time", "end_time", "win"]
    data = []
    for i in collection_of_results:
        data.append([i.puzzle.rating, i.total_seconds, i.start_time, i.end_time, i.win])
    return pd.DataFrame(data, columns=columns)

def get_puzzle_by_id(db_session, puzzle_id):
    return db_session.query(Puzzle).get(puzzle_id)

def get_puzzle(db_session, user):
    """
    1) Randomly pull out the puzzle category
    2) Get all history of that particular puzzle type
    3) Depending on the history of the execution, determine result
    4) Get a new puzzle from the specified interval
    """
    puzzle = None
    previous_results = get_previous_results(db_session, user)
    df = load_dataframe_from_collection_of_results(previous_results)
    while not puzzle:
        category = get_random_puzzle_category(db_session)
        print(len(df.index))
        rating = get_appropriate_rating(df)
        print("RATING:")
        print(rating)
        rating_interval = 10
        min_rating = rating - rating_interval
        max_rating = rating + rating_interval
        puzzles = get_new_puzzles(db_session, min_rating=min_rating, max_rating=max_rating, puzzle_category=category)
        puzzles = list(puzzles)
        puzzle = random.choice(puzzles) # TODO improve the performance of random selection
        return puzzle
    return puzzle

def get_new_puzzle_session(db_session, user):
    # puzzle = get_old_or_new(db_session)
    puzzle = get_puzzle(db_session, user)
    session = create_new_puzzle_session(db_session, puzzle)
    return session

# -- Old but new: ---

def get_puzzle_results(db_session, user=None, _date=None):
    query = db_session.query(Result)\
        .filter(Result.user == user)\
        .order_by(Result.start_time)
    if _date:
        query = query.filter(Result.start_time>=_date).filter(Result.start_time<_date+timedelta(days=1))
    return query

def store_puzzle_result(db_session, user, puzzle_result):
    import json
    d = json.loads(puzzle_result.json())
    d['start_time'] = convert_int_to_date(d['start_time'])
    d['end_time'] = convert_int_to_date(d['end_time'])
    result = Result(**d)
    result.user = user
    db_session.add(result)
    db_session.commit()
    # next_date = get_next_date_for_puzzle(db_session, result.puzzle_id, sri.algorithm, current_date=result.start_time)
    # schedule_puzzle(db_session, result.puzzle_id, next_date)
    # db_session.commit()
    return
