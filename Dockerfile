FROM python:3.8

# Installing chrome for selenium
# Adding trusting keys to apt for repositories
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

# Adding Google Chrome to the repositories
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

# Updating apt to see and install Google Chrome
RUN apt-get -y update

# Magic happens
RUN apt-get install -y google-chrome-stable

# Set display port as an environment variable
ENV DISPLAY=:99

# Stop chrome installation

# COPY ./src /app/src
COPY ./app /app/app

COPY ./requirements.txt /app

WORKDIR /app

RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt

CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 --forwarded-allow-ips='*' --timeout 0 -k uvicorn.workers.UvicornWorker app.api:app 
