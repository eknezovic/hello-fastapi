
from typing import Optional, List

from pydantic import BaseModel

from app.enums import EventEnum


class EventModel(BaseModel):
    timestamp: int
    event: EventEnum
    text: Optional[str]

class EventsModel(BaseModel): # FIXME to PuzzleEventsModel
    session_id: int
    puzzle_id: str
    events: List[EventModel]


class AuthDetails(BaseModel):
    username: str
    password: str

