
import os, time
from selenium import webdriver
from selenium.webdriver.common.by import By

from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.keys import Keys

from selenium.webdriver.support.ui import WebDriverWait

from selenium.webdriver.chrome.options import Options



def test_e2e():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    DRIVER_PATH = dir_path + "/chromedriver"

    chrome_options = Options()
    # chrome_options.add_argument("--disable-extensions")
    # chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox") # linux only
    chrome_options.add_argument("--headless")

    browser = webdriver.Chrome(DRIVER_PATH, options=chrome_options)

    base_url = os.getenv("TEST_APP_URL", "http://localhost:8000")
    login_url=f"{base_url}/login"

    browser.get(login_url)

    search_bar = browser.find_element("name", "username")
    search_bar.clear()
    search_bar.send_keys("string")

    search_bar = browser.find_element("name", "password")
    search_bar.clear()
    search_bar.send_keys("string")
    search_bar.send_keys(Keys.RETURN)

    time.sleep(5)

    assert "/home" in browser.current_url

    browser.find_element("id", "submit-val").click()

    time.sleep(5)
    assert "/puzzle" in browser.current_url


    def is_element_there(webdriver, by, identifier):
        try:
            webdriver.find_element(by, identifier)
        except NoSuchElementException:
            return False
        return True

    found = False
    for i in range(20):
        if is_element_there(browser, By.ID, "myBoard") and is_element_there(browser, By.ID, "candidateMoveInput"):
            print(f"Found after roughly {i} seconds...")
            break
        else:
            time.sleep(1)

    browser.get(f"{base_url}/dashboard")
    time.sleep(40)

    rect_elements = browser.find_elements(By.TAG_NAME, "rect")
    assert len(rect_elements) > 17 

    time.sleep(2)
    browser.close()