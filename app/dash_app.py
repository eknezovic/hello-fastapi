
import flask
import plotly.express as px
import pandas as pd
import requests

from dash import Dash, dcc, html, Input, Output
from pandas import Timestamp
from functools import lru_cache

dash_app = Dash(__name__, requests_pathname_prefix='/dashboard/', title='Dashboard')

dash_app.layout = html.Div(children=[
    html.H1(children='Your chess training dashboard'),
    html.Div(children='''
        Powered by Dash: A web application framework for your data.
    '''),
    dcc.Graph(
        id='example-graph',
    ),
    dcc.Slider(0, 100, 1,
               value=10, marks=None,
               tooltip={"placement": "bottom", "always_visible": True},
               id='my-slider'
    ),
])

def create_fig(df):
    fig = px.bar(df.sort_index(ascending=False), 
        x=df.index, y="rating", color="total_time",
        color_continuous_scale=["red", "purple", "blue"][::-1],
        pattern_shape="win",
        pattern_shape_map={False: "x", True: ""}
        ) 
    layout_yaxis_range=[df["rating"].min()-200, df["rating"].max()+10]
    fig.update_yaxes(range = layout_yaxis_range)
    return fig

@lru_cache
def get_df(api_url, token):
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.get(api_url, headers=headers)
    data = response.json()
    df = pd.DataFrame(data["dataframe"])
    return df

@dash_app.callback(
    Output('example-graph', 'figure'),
    Input('my-slider', 'value'),
)
def update_output_div(input_value):
    get_previous_results_url = f"{flask.request.host_url}api/get-previous-results?limit=100"
    token = flask.request.cookies['session_token']
    main_df = get_df(get_previous_results_url, token)
    display_df = main_df.head(input_value)
    fig = create_fig(display_df)
    return fig

if __name__ == '__main__':
    dash_app.run_server(debug=True)

