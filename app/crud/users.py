
users = []

from app.models import User

def add_user(db_session, username, hashed_password):
    user = User(username=username, hashed_password=hashed_password)
    db_session.add(user)
    db_session.commit()
    """
    users.append({
        'username': username,
        'password': hashed_password    
    })
    """

def get_user(db_session, username):
    result = db_session.query(User).filter(User.username == username).first()
    print(result)
    return result
    """
    for user in users:
        if user['username'] == username:
            return user
    return None
    """
    