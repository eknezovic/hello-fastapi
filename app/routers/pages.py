
from fastapi import FastAPI, Depends, HTTPException
import fastapi
from fastapi import FastAPI
from fastapi import Request, Response
# from .auth import AuthHandler
from typing import Optional, List, Union

from requests import Session
from fastapi import APIRouter, Depends

from app.pydantic_models import AuthDetails
from app.routers import auth

router = APIRouter()

import app.database as database

# app = FastAPI()
import jwt
from fastapi import HTTPException, Security
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from passlib.context import CryptContext
from datetime import datetime, timedelta


auth_handler = auth.AuthHandler()

from app.crud.users import add_user, get_user

from fastapi import Form
from fastapi.templating import Jinja2Templates
templates = Jinja2Templates(directory="app/templates/")

from fastapi import FastAPI, HTTPException, Depends, Request
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic import BaseModel

class Settings(BaseModel):
    authjwt_secret_key: str = "secret"
    # Configure application to store and get JWT from cookies
    authjwt_token_location: set = {"cookies"}
    # Disable CSRF Protection for this example. default is True
    authjwt_cookie_csrf_protect: bool = False

# TODO/FIXME: Add the middleware used for dashboard to all of this

from fastapi import Cookie, FastAPI

@AuthJWT.load_config
def get_config():
    return Settings()

@router.get("/login")
async def login_page(
    request: Request
):
    context = {
        'request': request,
    }
    return templates.TemplateResponse("login.html", context=context)

from app.routers.auth import crud_login

@router.post("/login")
async def login_post(
    # request: Request,
    username: str = Form(default=""), password: str = Form(default=""),
    db: Session = fastapi.Depends(database.get_db),
    # db: Session = fastapi.Depends(get_db)
    ): 
    print("A hit is a hit!")
    result = crud_login(db, username, password)
    if result:
        rr = fastapi.responses.RedirectResponse('/', status_code=303)
        rr.set_cookie(key="session_token", value=result['token'].decode())
        print("Result token:")
        print(result['token'])
        return rr
    else:
        return {"username": username}

from fastapi.responses import HTMLResponse

@router.get("/welcome")
async def test_welcome_page(
        request: Request,
        session_token: Union[str, None] = Cookie(default=None)
        # id_token: KeycloakIDToken = Security(auth.required)
):
    print("Hello?")
    print(session_token)
    # print("Decoded:")
    # print(auth_handler.auth_wrapper(session_token))
    import requests
    context = {
        'request': request,
    }
    return templates.TemplateResponse("welcome.html", context=context)


@router.get("/")
async def root(
    request: Request,
    session_token: Union[str, None] = Cookie(default=None)
    # username=fastapi.Depends(auth.auth_handler.auth_wrapper)
    ):
    return fastapi.responses.RedirectResponse('/home', status_code=302)

@router.get("/get-daily-results")
async def dashboard_view(response: Response):
    """
    Returns datetime list (or whatever will be necessary) to display the chart
    """
    db_session = database.get_db()
    results = crud.get_puzzle_results(db_session, crud.get_today())

    results = list(results)
    print(results)

    response = {
        'results': results
    }

    return response


import random

@router.get("/puzzle")
async def puzzle(
    request: Request,
    session_token: Union[str, None] = Cookie(default=None)
    ):
    db_session = database.get_db()
    print(auth_handler.decode_token(session_token))
    # puzzle = crud.get_old_or_new(db_session)
    context = {
        'request': request,
        # 'session_token': session_token
    }
    print(context['request'])
    return templates.TemplateResponse("puzzle.html", context=context)


@router.get("/get-puzzle")
async def get_puzzle():
    db_session = database.get_db()
    unfinished_puzzle_id_set = list(crud.get_unfinished_scheduled_puzzles_set(db_session))
    if unfinished_puzzle_id_set:
        puzzle_id = random.choice(unfinished_puzzle_id_set)
        url = "https://lichess.org/training/" + puzzle_id
    else:
        url = "/review-finished"
    return fastapi.responses.RedirectResponse(url, status_code=status.HTTP_302_FOUND)


@router.get("/review-finished")
async def review_finished():
    return {"message": "Review finished!"}
