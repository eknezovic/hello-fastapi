
from fastapi import FastAPI, Depends, HTTPException
# from .auth import AuthHandler

from requests import Session
from fastapi import APIRouter, Depends

router = APIRouter()

from app.pydantic_models import AuthDetails
import app.database as database

# app = FastAPI()

# copy pasta, mb move this to different file?


import jwt
from fastapi import HTTPException, Security
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from passlib.context import CryptContext
from datetime import datetime, timedelta

class AuthHandler():
    security = HTTPBearer()
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    secret = 'SECRET'

    def get_password_hash(self, password):
        return self.pwd_context.hash(password)

    def verify_password(self, plain_password, hashed_password):
        return self.pwd_context.verify(plain_password, hashed_password)

    def encode_token(self, user_id):
        payload = {
            'exp': datetime.utcnow() + timedelta(days=0, hours=12, minutes=0),
            'iat': datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            self.secret,
            algorithm='HS256'
        )

    def decode_token(self, token):
        print("Token: ")
        print(token)
        try:
            payload = jwt.decode(token, self.secret, algorithms=['HS256'])
            return payload['sub']
        except jwt.ExpiredSignatureError:
            print("Expired!")
            raise HTTPException(status_code=401, detail='Signature has expired')
        except jwt.InvalidTokenError as e:
            print("Invalid token!")
            raise HTTPException(status_code=401, detail='Invalid token')

    def auth_wrapper(self, auth: HTTPAuthorizationCredentials = Security(security)):
        print("Auth credentials:")
        print(auth.credentials)
        return self.decode_token(auth.credentials)

# eof


auth_handler = AuthHandler()

from app.crud.users import add_user, get_user

@router.post('/api/register', status_code=201)
def register(
    auth_details: AuthDetails, 
    db: Session = Depends(database.get_db)
):
    if get_user(db, auth_details.username):
        raise HTTPException(status_code=400, detail='Username is taken')
    hashed_password = auth_handler.get_password_hash(auth_details.password)
    add_user(db, auth_details.username, hashed_password)
    return


@router.post('/api/login')
def login(auth_details: AuthDetails,
    db: Session = Depends(database.get_db)
):
    result = crud_login(db, auth_details.username, auth_details.password)
    if not result:
        raise HTTPException(status_code=401, detail='Invalid username and/or password')
    return result


def crud_login(db, username, password):
    user = get_user(db, username)
    if (user is None) or (not auth_handler.verify_password(password, user.hashed_password)):
        return None
    token = auth_handler.encode_token(user.username)
    return { 'token': token }


@router.get('/api/unprotected')
def unprotected():
    return { 'hello': 'world' }


@router.get('/api/protected')
def protected(username=Depends(auth_handler.auth_wrapper)):
    return { 'name': username }