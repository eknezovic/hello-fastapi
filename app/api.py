
import time

from typing import Optional, List, Union

import fastapi
from fastapi import FastAPI
from fastapi import Request
from fastapi.middleware.cors import CORSMiddleware

from pydantic import BaseModel
from requests import Session
from starlette import status
from starlette.responses import FileResponse, Response

from fastapi.staticfiles import StaticFiles

from fastapi.templating import Jinja2Templates

from fastapi import Form

from fastapi import FastAPI, Depends, HTTPException
from fastapi.middleware.wsgi import WSGIMiddleware
from flask import Flask, escape, request
from starlette.routing import Mount
from starlette.types import Scope, Receive, Send


import app.crud as crud
import app.database as database
from app.pydantic_models import EventsModel

"""
auth = Auth(
    openid_connect_url="http://localhost:8080/auth/realms/test-realm/.well-known/openid-configuration",
    issuer="http://localhost:8080/auth/realms/test-realm",  # optional, verification only
    client_id="test-client",  # optional, verification only
    scopes=["good-service"],  # optional, verification only
    grant_types=[GrantType.PASSWORD],  # optional, docs only
    idtoken_model=KeycloakIDToken,  # optional, verification only
)
"""

from fastapi.middleware.wsgi import WSGIMiddleware
from app.dash_app import dash_app
from app.home_dash import dash_app as home_dash_app
import uvicorn

app = FastAPI(
    title="Example",
    version="dev",
    dependencies=[],
)

from app.routers import auth, pages

auth_handler = auth.AuthHandler()

from fastapi import HTTPException, Security

class AuthWSGIMiddleware(WSGIMiddleware):

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        #_, authorization = next((header for header in scope['headers'] if header[0] == b'authorization'), (b'authorization', "" ))
        print("Scope:")
        token = None
        for header in scope["headers"]:
            if header[0].decode() == "cookie":
                for item_value in header[1].decode().split("; "):
                    token = item_value.split("=")[1]
        # authenticate(authorization.decode('utf-8'))

        print("OUR TOKEN wooot wooot:")
        print(token)
        try:
            print(auth_handler.decode_token(token))
            print("YOU'RE IN, SON!!")
        except HTTPException:
            print("FAILED NOT LOGGED IN")
            raise HTTPException(status_code=401, detail='TODO - Should redirect to login')
        await super().__call__(scope, receive, send)

# routes = [
#             Mount("/v1", AuthWSGIMiddleware(flask_app)),
#         ]

app.mount("/home", AuthWSGIMiddleware(home_dash_app.server))
app.mount("/dashboard", AuthWSGIMiddleware(dash_app.server))

app.include_router(pages.router)
app.include_router(auth.router)

templates = Jinja2Templates(directory="app/templates")
app.mount("/static", StaticFiles(directory="app/static"), name="static")
app.mount("/img", StaticFiles(directory="app/static/img"), name="img")
app.mount("/js", StaticFiles(directory="app/static/js"), name="js")
app.mount("/css", StaticFiles(directory="app/static/css"), name="css")


origins = [
    "https://lichess.org",
    "http://localhost:1818",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins= ["*"],
    # allow_origins= origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

"""
@app.exception_handler(401)
async def custom_404_handler(_, __):
    return RedirectResponse("/login")

@app.exception_handler(403)
async def custom_404_handler(_, __):
    return RedirectResponse("/login")
"""

from fastapi.responses import RedirectResponse

@app.get("/api/daily-summary")
async def get_data(
        request: Request,
        response: Response,
        # id_token: KeycloakIDToken = Security(auth.required)
):
    db_session = database.get_db()
    today = crud.get_today()
    # new_puzzles = crud.get_new_puzzle_results_only(db_session, today)
    # scheduled_puzzles = crud.get_scheduled_puzzles(db_session, today)
    # unfinished_puzzles = crud.get_unfinished_scheduled_puzzles_set(db_session, today)
    response = {
        'new_count': 0, # new_puzzles.count(),
        'unfinished_count': 19, # len(unfinished_puzzles),
        "finished_review_count": 23, # len(scheduled_puzzles) - len(unfinished_puzzles),
        "total_review_count": 40 #len(scheduled_puzzles)
    }
    return response


from fastapi.responses import HTMLResponse


import datetime

class PostPuzzleResult(BaseModel): # TODO refactor unions
    puzzle_id: str = "tNbGF" # TODO determine impact of setting this default
    start_time: Union[int, datetime.datetime] 
    end_time: Union[int, datetime.datetime]
    win: bool

class GetPuzzleResults(BaseModel):
    puzzle_results: List[PostPuzzleResult]

@app.post("/api/store-puzzle-result")
async def store_puzzle_result_api(
        data: PostPuzzleResult,
        # id_token: KeycloakIDToken = Security(auth.required)
        db: Session = fastapi.Depends(database.get_db),
        username=fastapi.Depends(auth.auth_handler.auth_wrapper)
):
    user = crud.users.get_user(db, username)
    # db_session = database.get_db()
    crud.store_puzzle_result(db, user, data)
    return {"message": data.puzzle_id}


@app.get("/api/get-previous-results")
async def api_get_previous_results(
    request: Request,
    response: Response,
    db: Session = fastapi.Depends(database.get_db),
    username=fastapi.Depends(auth.auth_handler.auth_wrapper),
    limit: int = None
):
    user = crud.users.get_user(db, username)
    q = crud.get_previous_results(db, user, limit)
    df = crud.load_dataframe_from_collection_of_results(q)
    # df = df.head(limit)
    print(df.shape)
    return {
        'dataframe': df.to_dict("list")
    }

@app.get("/api/get-random-puzzle")
async def api_get_random_puzzle(
    request: Request,
    response: Response,
    min_rating: int,
    max_rating: int,
    db: Session = fastapi.Depends(database.get_db),
):
    puzzle = crud.get_random_puzzle(db, min_rating=min_rating, max_rating=max_rating)
    return {
        "puzzle_id": puzzle.id
    }

@app.get("/api/get-puzzle")
async def api_get_puzzle(
    request: Request,
    response: Response,
    db: Session = fastapi.Depends(database.get_db),
    username=fastapi.Depends(auth.auth_handler.auth_wrapper),
    puzzle_id = fastapi.Query(None, alias="id")
):
    user = crud.users.get_user(db, username)
    if not puzzle_id:
        puzzle = crud.get_puzzle(db, user)
    else:
        print("HELLO PUZZLE_ID")
        print(puzzle_id)
        puzzle = crud.get_puzzle_by_id(db, puzzle_id)
    response = {
        # 'session_id': puzzle_session.id,
        'puzzle_id': puzzle.id,
        'fen': puzzle.fen,
        'rating': puzzle.rating,
        'moves': puzzle.moves
    }
    return response

@app.get("/api/get-results")
async def api_get_results(
    db: Session = fastapi.Depends(database.get_db),
    username=fastapi.Depends(auth.auth_handler.auth_wrapper)
    ):
    # return { 'name': username }
    user = crud.users.get_user(db, username)
    puzzle_results = list(crud.get_puzzle_results(db, user=user))
    for i in puzzle_results:
        print(i.__dict__)
    return GetPuzzleResults(puzzle_results=[PostPuzzleResult(**i.__dict__) for i in puzzle_results])

@app.post("/api/delete-test-user")
async def delete_user(
    db: Session = fastapi.Depends(database.get_db)
    ):
    from app.models import User
    from app.crud.users import get_user

    test_user = get_user(db, "string")

    if test_user:
        db.delete(test_user)
        db.commit()

@app.post("/api/store-events")
async def store_events(
        data: EventsModel,
        # id_token: KeycloakIDToken = Security(auth.required)
    db: Session = fastapi.Depends(database.get_db)

):
    crud.store_events(db, data)
    return {"message": "Data saved!"}


