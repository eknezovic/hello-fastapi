
import os

from datetime import datetime, date
import requests

import pytz


tz = pytz.timezone('Europe/Prague')

CURRENT_TIME = 1629902144963

def get_secs(n):
    number = CURRENT_TIME + n * 60 * 60 * 24 * 1000
    return number

def get_date(n):
    number = get_secs(n)
    date = convert_int_to_date(number)
    return date.date()

def convert_int_to_date(number):
    epoch = number / 1000
    utc_date = datetime.fromtimestamp(epoch, tz=tz)  # outputs: datetime date object
    return utc_date


def test_user_workflow():
    # Arrange
    data = {
        "username": "string",
        "password": "string"
    }
    base_url = os.getenv("TEST_APP_URL", "http://localhost:8000")
    register_url= f"{base_url}/api/register"
    login_url = f"{base_url}/api/login"
    protected_url = f"{base_url}/api/protected"
    unprotected_url = f"{base_url}/api/unprotected"

    get_puzzle_url = f"{base_url}/api/get-puzzle"
    get_results_url = f"{base_url}/api/get-results"
    store_puzzle_result_url = f"{base_url}/api/store-puzzle-result"

    delete_test_user_url = f"{base_url}/api/delete-test-user"

    get_previous_results_url = f"{base_url}/api/get-previous-results"

    response = requests.post(delete_test_user_url)
    assert response.status_code == 200

    # Act & Assert 
    response = requests.get(unprotected_url)
    assert response.status_code == 200

    response = requests.get(protected_url)
    assert response.status_code == 403

    response = requests.post(register_url, json=data)
    assert response.status_code == 201

    response = requests.post(register_url, json=data)
    assert response.status_code == 400

    response = requests.post(login_url, json=data)
    assert response.status_code == 200
    token = response.json()['token']

    headers = {
        "Authorization": f"Bearer {token}"
    }

    response = requests.get(unprotected_url, headers=headers)
    assert response.status_code == 200

    response = requests.get(protected_url, headers=headers)
    assert response.status_code == 200

    response = requests.get(get_results_url, headers=headers)
    assert response.status_code == 200
    data = response.json()
    assert len(data["puzzle_results"]) == 0

    puzzle_id = "nNWxM"
    response = requests.get(f"{base_url}/api/get-puzzle?id={puzzle_id}", headers=headers)
    assert response.status_code == 200
    assert response.json()['puzzle_id'] == puzzle_id
    assert response.json()['fen'] == '2k2r1N/2p3pp/p1pb1n2/4p3/2q1P1bQ/2N5/PPP3PP/R1B2R1K w - - 1 18'

    response = requests.get(f"{base_url}/api/get-random-puzzle?min_rating=1500&max_rating=1600", headers=headers)
    assert response.status_code == 200
    puzzle_id = response.json()['puzzle_id']
    response = requests.get(f"{base_url}/api/get-puzzle?id={puzzle_id}", headers=headers)
    assert response.status_code == 200
    data = response.json()
    assert data["rating"] >= 1500
    assert data["rating"] <= 1600

    response = requests.get(get_puzzle_url, headers=headers)
    assert response.status_code == 200
    data = response.json()

    the_time = get_secs(0)
    the_date = get_date(0)

    puzzle_id = data["puzzle_id"]

    winning_puzzle_data = {
        "puzzle_id": puzzle_id,
        "start_time": the_time,
        "end_time": the_time + 30,
        "win": True
    }

    response = requests.post(
        store_puzzle_result_url,
        headers=headers,
        json=winning_puzzle_data
        )
    assert response.status_code == 200

    response = requests.get(get_results_url, headers=headers)
    assert response.status_code == 200
    data = response.json()
    assert len(data["puzzle_results"]) == 1
 
    losing_puzzle_data = {
        "puzzle_id": puzzle_id,
        "start_time": the_time + 60,
        "end_time": the_time + 90,
        "win": False
    }

    response = requests.post(
        store_puzzle_result_url,
        headers=headers,
        json=losing_puzzle_data,
        )
    assert response.status_code == 200

   
    response = requests.get(get_previous_results_url, headers=headers)
    assert response.status_code == 200
    data = response.json()

    import pandas as pd
    df = pd.DataFrame(data["dataframe"])
    df.shape[1] == 2

