
async function run() {
    const dailySummaryUrl = "/api/daily-summary"
    // const token = keycloak.token
    // console.log("Token: ", token)
    var response = await fetch(dailySummaryUrl, {
        headers: {
            //	'Authorization': `Bearer ${token}`
        }
    })
    var data = await response.json()

    console.log("HERE COMES DATA:", data)
    document.getElementById("new-count").innerText = data["new_count"]
    document.getElementById("finished-review-count").innerText = data["finished_review_count"]
    document.getElementById("total-review-count").innerText = data["total_review_count"]

    // document.getElementById("user-first-name").innerText = keycloak.idTokenParsed['given_name']
    document.getElementById("user-first-name").innerText = "Eduard"
}

