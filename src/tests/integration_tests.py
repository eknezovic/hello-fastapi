
import requests

import os

def test_main():
    url = os.getenv("APP_URL")
    response = requests.get(url)
    data = response.json()
    assert data["message"] == "VICTORY!!!"
