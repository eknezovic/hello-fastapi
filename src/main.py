import os
import uvicorn
from fastapi import FastAPI

from src.service import hello

app = FastAPI()

@app.get("/")
def index():
    message = hello()
    return {"message": message}

if __name__ == "__main__":
    uvicorn.run("main:app", port=int(os.environ.get("PORT", 8080)), debug=True)
