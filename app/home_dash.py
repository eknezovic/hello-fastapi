

from dash import Dash, html, dcc
from dash import Dash, dcc, html, Input, Output

from dash import Dash, dcc, html, Input, Output, State



import plotly.express as px
import pandas as pd

import flask

dash_app = Dash(__name__, requests_pathname_prefix='/home/', title='Main')


dash_app.layout = html.Div([
    html.H1("Chess Tactics Trainer"),
    html.H3("Set the desired difficulty:"),
    dcc.RangeSlider(min=600, max=2900, marks=None, value=[1400, 1600], 
    tooltip={"placement": "bottom", "always_visible": True},
    id='my-range-slider'),
    html.Div(id='output-container-range-slider'),
    html.Button('Let\'s do it!', id='submit-val', n_clicks=0),
    dcc.Location(
        id="url", 
        # href="http://localhost:8000/home/"
        pathname="/home/"
    )

])

from flask import redirect
import webbrowser


@dash_app.callback(
    Output('output-container-range-slider', 'children'),
    [Input('my-range-slider', 'value')])
def update_output(value):
    return 'You have selected "{}"'.format(value)

@dash_app.callback(
    Output('url', 'href'),
    # Input('my-range-slider', 'slider_value'),
    Input('submit-val', 'n_clicks'),
    State('my-range-slider', 'value')
)
def update_output(n_clicks, value):
    if not n_clicks:
        return None
    else:
        url = f"/puzzle?min_rating={value[0]}&max_rating={value[1]}"
        return url


if __name__ == '__main__':
    dash_app.run_server(debug=True)

