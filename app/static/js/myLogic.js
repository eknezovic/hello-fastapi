

var keycloak = new Keycloak("http://localhost:8000/static/keycloak.json");

async function initKeycloak() {
    await keycloak.init({onLoad: 'login-required'}).then(function() {
        console.log("Welcome token!!")
    }).then(() => {
        document.getElementById("body-div").classList.remove("hidden")
    }).catch((error) => {
        document.getElementById("error-div").classList.remove("hidden")
    });
}

function constructTableRows(keycloakToken) {
    document.getElementById('row-username').innerHTML = keycloakToken.preferred_username;
    document.getElementById('row-firstName').innerHTML = keycloakToken.given_name;
    document.getElementById('row-lastName').innerHTML = keycloakToken.family_name;
    document.getElementById('row-name').innerHTML = keycloakToken.name;
    document.getElementById('row-email').innerHTML = keycloakToken.email;
}

function pasteToken(token){
    document.getElementById('ta-token').value = token;
    document.getElementById('ta-refreshToken').value = keycloak.refreshToken;
}

var refreshToken = function() {
    keycloak.updateToken(-1)
    .then(function(){
        document.getElementById('ta-token').value = keycloak.token;
        document.getElementById('ta-refreshToken').value = keycloak.refreshToken;
    });
}

var logout = function() {
    keycloak.logout({"redirectUri":"/login"});
}